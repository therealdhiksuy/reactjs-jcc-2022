import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ButtonSwitchColor from "../component/ButtonSwitchColor";
import { MahasiswaContext } from "../context/MahasiswaContext";


const Tugas14List= ()=> {
    const {state,handleFunction} = useContext(MahasiswaContext)
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state

    const {
      handleIndexScore,
      handleSubmit,
      handleEdit,
      handleChange,
      handleDelete
    } = handleFunction

      useEffect(()=>{

        let fetchData = async () => {
            let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
        let result = data.map((res)=>{
            let{id, 
                name , 
                course, 
                score, } = res
            return {
                id, 
                name , 
                course, 
                score, }
        })
        
          setDataMahasiswa([...result])
        
           
        }
        if (fetchStatus){
            fetchData()
            setFetchStatus(false)

        }
        
      },[fetchStatus,setFetchStatus])
      
     
      
  

    return(

        <>

 <div className="mt-20 min-g-screen flex items-center justify-center">
 <ButtonSwitchColor/>

 </div>
        
        <div className="container">
            <h1>Daftar Nilai Mahasiswa</h1>
        <table id="table-data">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Mata Kuliah</th>
    <th>Nilai</th>
    <th>Indeks Nilai</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
 
      {
      dataMahasiswa.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.name}</td>
    <td>{res.course}</td>
    <td>{res.score}</td>
    <td>{handleIndexScore(res.score)}</td>
  
    <td>
        <button onClick={handleEdit} value={res.id}>Edit</button>
        <button onClick={handleDelete} value={res.id}>Delete</button>

    </td>
    
  </tr>
           
          )
      })
  }
 
  </tbody>

</table>
<Link to='/tugas14/create'><button style={{
            marginLeft:"40%",marginTop:"20px"
        }}> Create Data </button></Link>

        </div>
        
       
        </>
    )

}
export default Tugas14List