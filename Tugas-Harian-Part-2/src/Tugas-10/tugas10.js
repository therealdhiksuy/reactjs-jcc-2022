import React from "react"
import logo from '../img/logo.png';


const Tugas10 = () => {
  const ListCheckBox = (props) =>{
    return (
      <>
      <div className="list">
      
      <input className="check" type={'checkbox'}/> {props.text}
      
      </div>
      

      
      
      </>
    )
  }

  return(

    <>
    <div className="card">
      <img src={logo} />
      <h1> THINGS TO DO</h1>
      <p>During Bootcamp in Jabarcodingcamp</p>
      
      <div className="containerList">
        <ListCheckBox text ={"Belajar Git dan CLI"}/> 
        <ListCheckBox text ={"Belajar HTML dan CSS"}/> 
        <ListCheckBox text ={"Belajar Javascript"}/> 
        <ListCheckBox text ={"Belajar ReactJs Dasar"}/> 
        <ListCheckBox className="w-full" text ={"Belajar ReactJs Advanced"}/> 
        <br/>
        

      </div>
    <button className="mt-10 py-2 px-4 flex justify-center items-center  bg-blue-500 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">Send</button>
    </div>
    </>
  )
}

export default Tugas10;
