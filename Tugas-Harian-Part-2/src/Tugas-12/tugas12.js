import axios from "axios"
import React, { useEffect, useState } from "react";


const Tugas12= ()=> {
    let [dataMahasiswa,setDataMahasiswa] = useState([]) 
      const [currentId, setCurrentId] = useState(-1)
      const [fetchStatus ,setFetchStatus] = useState(true)
      const [input, setInput] = useState({
          name:"",
          course:"",
          score:0
      })
      useEffect(()=>{

        let fetchData = async () => {
            let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
        let result = data.map((res)=>{
            let{id, 
                name , 
                course, 
                score, } = res
            return {
                id, 
                name , 
                course, 
                score, }
        })
        
          setDataMahasiswa([...result])
           
        }
        if (fetchStatus){
            fetchData()
            setFetchStatus(false)

        }
        
      },[fetchStatus,setFetchStatus])
      
      const handleDelete = (event) => {
        let dataId = parseInt(event.target.value)
        // console.log(dataid)
        axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${dataId}`)
        .then((res)=>{

            // console.log(res)
            setFetchStatus(true)
        })

        .catch((err)=>{
            console.log(err)
        })

       
       
       

      }
      const handleEdit = (event) => {
        let dataId = parseInt(event.target.value)
       
        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${dataId}`)
        .then((res)=>{
           let data = res.data

            setInput({
                name:data.name,
                course:data.course,
                score:data.score
              }

            )
            setCurrentId(data.id)

           
        })

        .catch((err)=>{
            console.log(err)
        })
        // let editValue = dataMahasiswa[dataId]
        // setInput(editValue)
        // setCurrentId(event.target.value)

        
  
       
      }
      const handleChange =(event)=>{
        let name = event.target.name 
        let value = event.target.value
        
  setInput({...input,[name]:value})
        

      }
      const handleSubmit = (event) => {
          event.preventDefault()
          
          let {name,course,score} = input
          let data = dataMahasiswa

        //   let data = dataBuah
          
          if(currentId===-1){
              axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name,course,score})

              .then((res)=>{

               setFetchStatus(true)
              })

            // data = [...data,{nama , hargaTotal , beratTotal}]
            
          }else {
            axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,input)
            .then((res)=>{

                setFetchStatus(true)
               })
          }
          setInput ({
            name:"",
            course:"",
            score:0
          })
          setCurrentId(-1)
   
        //   setDataBuah([...data])
      }
      
    
      const handleIndexScore = (param)=>{
        if(param>=80){
            return "A"
        }
        else if (param>=70 && param<80 ){
            return "B"
        }
        else if(param>=60 && param <70){
            return "C"
        }
        else if (param>=50 && param <60){
            return "D"
        }
        else {
            return "E"
        }
    }
      
  

    return(

        <>
        
        <div className="container">
            <h1>Daftar Nilai Mahasiswa</h1>
        <table id="table-data">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Mata Kuliah</th>
    <th>Nilai</th>
    <th>Indeks Nilai</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
 
      {
      dataMahasiswa.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.name}</td>
    <td>{res.course}</td>
    <td>{res.score}</td>
    <td>{handleIndexScore(res.score)}</td>
  
    <td>
        <button onClick={handleEdit} value={res.id}>Edit</button>
        <button onClick={handleDelete} value={res.id}>Delete</button>

    </td>
    
  </tr>
           
          )
      })
  }
 
  </tbody>

</table>

        </div>
        
        <div className="container-form">
        <h1>Form  Nilai Mahasiswa</h1>
            
  <form onSubmit={handleSubmit}>
    <label >Nama :</label>
    <input onChange={handleChange} value={input.name} type="text"  name="name" placeholder="Nama"required/>
    <label >Mata Kuliah :</label>
    <input onChange={handleChange} value={input.course} type="text"  name="course" placeholder="Mata Kuliah" required/>
    <label >Nilai : </label>
    <input onChange={handleChange} value={input.score} type="number"  name="score" min={0} max={100}  placeholder="Nilai"  required/>
    

    
  
    <input type="submit" value="Submit"/>
  </form>
</div>
        </>
    )

}
export default Tugas12