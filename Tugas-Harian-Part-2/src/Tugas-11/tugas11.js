import React, { useState } from "react";
import "../css/tugas11.css";

const Tugas11= ()=> {
    let [dataBuah,setDataBuah] = useState([
        {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
        {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
        {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
        {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
      ]) 
      const [currentIndex, setCurrentIndex] = useState(-1)
      const [input, setInput] = useState({
          nama:"",
          hargaTotal:0,
          beratTotal:0
      })
      const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = dataBuah[index]
        let data = dataBuah.filter((e) => {return e !== deletedItem})
        console.log(data)
        setDataBuah(data)
      }
      const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editValue = dataBuah[index]
        setInput(editValue)
        setCurrentIndex(event.target.value)

        
  
       
      }
      const handleChange =(event)=>{
        let name = event.target.name 
        let value = event.target.value
        
  setInput({...input,[name]:value})
        

      }
      const handleSubmit = (event) => {
          event.preventDefault()
          let {nama,hargaTotal,beratTotal} = input
          let data = dataBuah
          
          if(currentIndex===-1){
            data = [...data,{nama , hargaTotal , beratTotal}]
            
          }else {
              
            data[currentIndex] = input
            
          }
    
          setDataBuah([...data])
      }
    

      
  

    return(

        <>
        
        <div className="container">
            <h1>Daftar Harga Buah</h1>
        <table id="table-data">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Harga Total</th>
    <th>Berat Total</th>
    <th>Harga per Kg</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
  {
      dataBuah.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.nama}</td>
    <td>{res.hargaTotal}</td>
    <td>{res.beratTotal/1000} kg</td>
    <td>{res.hargaTotal/(res.beratTotal/1000)}</td>
    <td>
        <button onClick={handleEdit} value={index}>Edit</button>
        <button onClick={handleDelete} value={index}>Delete</button>

    </td>
    
  </tr>
           
          )
      })
  }
  </tbody>

</table>

        </div>
        
        <div className="container-form">
        <h1>Form Daftar Harga Buah</h1>
            
  <form onSubmit={handleSubmit}>
    <label >Nama :</label>
    <input onChange={handleChange} value={input.nama} type="text"  name="nama" placeholder="Nama"required/>
    <label >Harga Total :</label>
    <input onChange={handleChange} value={input.hargaTotal} type="number"  name="hargaTotal" placeholder="Total Harga" required/>
    <label >Berat Total (dalam gram) : </label>
    <input onChange={handleChange} value={input.beratTotal} type="number"  name="beratTotal" min="2000" placeholder="Total Berat"  required/>
    

    
  
    <input type="submit" value="Submit"/>
  </form>
</div>
        </>
    )

}
export default Tugas11