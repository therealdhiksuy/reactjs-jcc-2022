import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { MahasiswaContext } from "../context/MahasiswaContext";


const MahasiswaList= ()=> {
    const {state,handleFunction} = useContext(MahasiswaContext)
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state

    const {
      handleIndexScore,
      handleSubmit,
      handleEdit,
      handleChange,
      handleDelete,
      handleEditInForm

    } = handleFunction

      useEffect(()=>{

        let fetchData = async () => {
            let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
        let result = data.map((res)=>{
            let{id, 
                name , 
                course, 
                score, } = res
            return {
                id, 
                name , 
                course, 
                score, }
        })
        
          setDataMahasiswa([...result])
        
           
        }
        if (fetchStatus){
            fetchData()
            setFetchStatus(false)

        }

        
        
      },[fetchStatus,setFetchStatus])
      
     
      
  

    return(

        <>
        
        <div className="container">
            <h1>Daftar Nilai Mahasiswa</h1>
        <table id="table-data">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Mata Kuliah</th>
    <th>Nilai</th>
    <th>Indeks Nilai</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
 
      {
      dataMahasiswa.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.name}</td>
    <td>{res.course}</td>
    <td>{res.score}</td>
    <td>{handleIndexScore(res.score)}</td>
  
    <td>
        <button onClick={handleEditInForm} value={res.id}>Edit</button>
        <button onClick={handleDelete} value={res.id}>Delete</button>

    </td>
    
  </tr>
           
          )
      })
  }
 
  </tbody>

</table>

        </div>
        
       
        </>
    )

}
export default MahasiswaList