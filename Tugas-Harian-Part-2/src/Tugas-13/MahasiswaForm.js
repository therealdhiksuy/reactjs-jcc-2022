import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { MahasiswaContext } from "../context/MahasiswaContext";


const MahasiswaForm= ()=> {
    const {state,handleFunction} = useContext(MahasiswaContext)
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state
    const {
        handleIndexScore,
        handleSubmit,
        handleEdit,
        handleChange,
        handleDelete,
        handleSubmitForm
      } = handleFunction
      useEffect(()=>{


        
      },[])
      
      

    return(

        <>
        
        
        
        <div className="container-form">
        <h1>Form  Nilai Mahasiswa</h1>
            
  <form onSubmit={handleSubmitForm}>
    <label >Nama :</label>
    <input onChange={handleChange} value={input.name} type="text"  name="name" placeholder="Nama"required/>
    <label >Mata Kuliah :</label>
    <input onChange={handleChange} value={input.course} type="text"  name="course" placeholder="Mata Kuliah" required/>
    <label >Nilai : </label>
    <input onChange={handleChange} value={input.score} type="number"  name="score" min={0} max={100}  placeholder="Nilai"  required/>
    

    
  
    <input type="submit" value="Submit"/>
  </form>
</div>
        </>
    )

}
export default MahasiswaForm