import React from "react";
import { MahasiswaProvider } from "../context/MahasiswaContext";
import MahasiswaForm from "./MahasiswaForm";
import MahasiswaList from "./MahasiswaList";

const Mahasiswa= () => {
return (
    <>

  <MahasiswaList/>
  <MahasiswaForm/>

  </>
)

}

export default Mahasiswa