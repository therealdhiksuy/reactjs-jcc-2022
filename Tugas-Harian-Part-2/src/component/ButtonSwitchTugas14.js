import React, { useContext } from "react";
import { SwitchColorContext } from "../context/SwitchColor";
const ButtonSwitchCTugas14 = ()=>{
let {value,setValue} = useContext(SwitchColorContext)
let {buttonName,setButtonName} = useContext(SwitchColorContext)

let handleSwitch = ()=> {
    value ==="bg-white dark:bg-gray-800  shadow" ? setValue("bg-black dark:bg-gray-800  shadow") : setValue("bg-white dark:bg-gray-800  shadow");
    value ==="bg-white dark:bg-gray-800  shadow" ? setButtonName("Light") : setButtonName("Dark")
    
}
return (

    <>
     <button onClick={handleSwitch} type="button" className="ml-200 py-2 px-4   hover:bg-indigo-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
       {buttonName}
      </button>

    </>
)


}
export default ButtonSwitchCTugas14