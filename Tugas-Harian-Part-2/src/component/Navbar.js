import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { SwitchColorContext } from "../context/SwitchColor";
import logo from '../img/logo.png';
import ButtonSwitchColor from "./ButtonSwitchColor";
import Cookies from 'js-cookie'


const Navbar = () => {
  let history = useHistory()
let {
    value,setValue
} = useContext(SwitchColorContext)
    return (
        <>
        <header>
        <nav className= {value}>
          <div className="max-w-7xl mx-auto px-8">
            <div className="flex items-center justify-between h-16">
              <div className="w-full justify-between flex items-center">
                <a className="flex-shrink-0" href="/">
                  <img className="mb-4 h-20 w-48" src={logo} alt="Workflow" />
                </a>
                <div className="hidden md:block">
                  <div className="ml-10 flex items-baseline space-x-4">
        <ul>
        <div className="hidden md:block">
                  <div className="ml-10 flex items-baseline space-x-4">
        <li className="text-gray-300  hover:text-red-800 white:hover:text-white px-3 py-2 rounded-md text-sm font-bold mt-10" ><Link to ='/'>Tugas10</Link></li>
        <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to ='/tugas11'>Tugas11</Link></li>
        <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to ='/tugas12'>Tugas12</Link></li>
        <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to ='/tugas13'>Tugas13</Link></li>
        <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to ='/tugas14'>Tugas14</Link></li>
        <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to ='/tugas15'>Tugas15</Link></li>
        {!Cookies.get('token') && <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to={'/register'}>Register</Link></li>}
        {!Cookies.get('token') && <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><Link to={'/login'}>Login</Link></li>}
        {Cookies.get('token') && <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium"><span onClick={() => {
                    Cookies.remove('token')
                    window.location = "/login"

                }}>Logout</span></li>}
        <li className="text-gray-300  hover:text-red-800 dark:hover:text-white px-3 py-2 rounded-md text-sm font-medium ml-20"><ButtonSwitchColor/></li>
        
        
        </div>
        </div>
        </ul>
        </div>
        </div></div></div></div></nav></header>
      
</>
  
    )
}

export default Navbar