import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom"
import Login from "../Auth/Login";
import Register from "../Auth/Register";
import Navbar from "../component/Navbar";
import { MahasiswaProvider } from "../context/MahasiswaContext";
import { SwitchColorContextProvider } from "../context/SwitchColor";
import Tugas10 from '../Tugas-10/tugas10';
import Tugas11 from "../Tugas-11/tugas11";
import Tugas12 from "../Tugas-12/tugas12";
import Mahasiswa from "../Tugas-13/Mahasiswa";
import Tugas14Form from "../Tugas-14/Tugas14Form";
import Tugas14List from "../Tugas-14/Tugas14List";

import Tugas15Form from "../Tugas-15/Tugas15Form";
import Tugas15List from "../Tugas-15/Tugas15List";
const Routes = () => {


return (

    <>
    <BrowserRouter>
    <MahasiswaProvider>
        <SwitchColorContextProvider>
        <Navbar/>
    <Switch>

    
    <Route path={'/'} exact component={Tugas10} />
    <Route path={'/tugas11'} exact component={Tugas11} />
    <Route path={'/tugas12'} exact component={Tugas12} />
    <Route path={'/tugas13'} exact component={Mahasiswa} />
    <Route path={'/tugas14'} exact component={Tugas14List} />
    <Route path={'/tugas14/create'} exact component={Tugas14Form} />
    <Route path={'/tugas14/edit/:slug'} exact component={Tugas14Form} />
    <Route path={'/tugas15'} exact component={Tugas15List} />
    <Route path={'/tugas15/create'} exact component={Tugas15Form} />
    <Route path={'/tugas15/edit/:slug'} exact component={Tugas15Form} />
    <Route path={'/login'} exact component={Login} />
    <Route path={'/register'} exact component={Register} />
    


    

    </Switch>
    </SwitchColorContextProvider>
    </MahasiswaProvider>
    
    </BrowserRouter>
    </>
)

}

export default Routes