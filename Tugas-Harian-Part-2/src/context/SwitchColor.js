import React, { createContext, useState } from "react";
import axios from "axios"
import { useHistory } from "react-router-dom";

export const SwitchColorContext = createContext()
export const SwitchColorContextProvider = props =>{
let [value,setValue] = useState ('bg-white dark:bg-gray-800  shadow')
let [buttonName , setButtonName]=useState ('Dark')
 return (
     <SwitchColorContext.Provider value={{value,setValue,buttonName , setButtonName}}>

{props.children}
     </SwitchColorContext.Provider>
 )
}

