import React, { createContext, useState } from "react";
import axios from "axios"
import { useHistory } from "react-router-dom";
import Toast from "../component/Toast";
import ButtonSwitchColor from "../component/ButtonSwitchColor";

export const MahasiswaContext = createContext()
export const MahasiswaProvider = props =>{
    let history = useHistory()
    let [dataMahasiswa,setDataMahasiswa] = useState([]) 
    const [currentId, setCurrentId] = useState(-1)
    const [fetchStatus ,setFetchStatus] = useState(true)
    const [input, setInput] = useState({
        name:"",
        course:"",
        score:0
    })
    const handleDelete = (event) => {
        let dataId = parseInt(event.target.value)
        // console.log(dataid)
        axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${dataId}`)
        .then((res)=>{
          alert( "Data Berhasil Di Hapus")
            // console.log(res)
            setFetchStatus(true)
        })

        .catch((err)=>{
            console.log(err)
        })

       
       
       

      }
      const handleEdit = (event) => {
        let dataId = parseInt(event.target.value)
       history.push(`/tugas14/edit/${dataId}`)
        
        // let editValue = dataMahasiswa[dataId]
        // setInput(editValue)
        // setCurrentId(event.target.value)

        
        
       
      }
      const handleEdit15 = (event) => {
        let dataId = parseInt(event.target.value)
       history.push(`/tugas15/edit/${dataId}`)
        
        // let editValue = dataMahasiswa[dataId]
        // setInput(editValue)
        // setCurrentId(event.target.value)

        
        
       
      }
      const handleEditInForm = (event) => {

        let dataId = parseInt(event.target.value)
        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${dataId}`)
        .then((res)=>{
           let data = res.data

            setInput({
                name:data.name,
                course:data.course,
                score:data.score
              }

            )
            setCurrentId(data.id)

           
        })

        .catch((err)=>{
            console.log(err)
        })
        }
        


      
      const handleChange =(event)=>{
        let name = event.target.name 
        let value = event.target.value
        
  setInput({...input,[name]:value})
        

      }
      const handleSubmit = (event) => {
          event.preventDefault()
          
          let {name,course,score} = input
          let data = dataMahasiswa

        //   let data = dataBuah
          
          if(currentId===-1){
              axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name,course,score})

              .then((res)=>{
                history.push('/tugas14')
               setFetchStatus(true)
              })

            // data = [...data,{nama , hargaTotal , beratTotal}]
            
          }else {
            axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,input)
            .then((res)=>{
              history.push('/tugas14')
              
                setFetchStatus(true)
               })
          }
          setInput ({
            name:"",
            course:"",
            score:0
          })
          setCurrentId(-1)
    
        //   setDataBuah([...data])
      }
      const handleSucces = () => {
        return (
          <>
          
          </>
        )
      }
      const handleSubmitForm = (event) => {
        event.preventDefault()
        
        let {name,course,score} = input
        let data = dataMahasiswa

      //   let data = dataBuah
        
        if(currentId===-1){
            axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name,course,score})

            .then((res)=>{
              
             setFetchStatus(true)
            })

          // data = [...data,{nama , hargaTotal , beratTotal}]
          
        }else {
          axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,input)
          .then((res)=>{
           
            
              setFetchStatus(true)
             })
        }
        setInput ({
          name:"",
          course:"",
          score:0
        })
        setCurrentId(-1)
  
      //   setDataBuah([...data])
    }
    const handleSubmitForm15 = (event) => {
      event.preventDefault()
      
      let {name,course,score} = input
      let data = dataMahasiswa

    //   let data = dataBuah
      
      if(currentId===-1){
          axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,input)

          .then((res)=>{
            alert( "Data Berhasil Ditambah")
            history.push('/tugas15')
            
           setFetchStatus(true)
          })

        // data = [...data,{nama , hargaTotal , beratTotal}]
        
      }else {
        axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,input)
        .then((res)=>{
          history.push('/tugas15')
          alert( "Data Berhasil Diubah")
            setFetchStatus(true)
           })
      }
      setInput ({
        name:"",
        course:"",
        score:0
      })
      setCurrentId(-1)

    //   setDataBuah([...data])
  }
      
    
      const handleIndexScore = (param)=>{
        if(param>=80){
            return "A"
        }
        else if (param>=70 && param<80 ){
            return "B"
        }
        else if(param>=60 && param <70){
            return "C"
        }
        else if (param>=50 && param <60){
            return "D"
        }
        else {
            return "E"
        }
  }  

  

 let state = {
     dataMahasiswa,setDataMahasiswa,
     currentId, setCurrentId,
     fetchStatus ,setFetchStatus,
     input, setInput

 }
 let handleFunction ={
    handleIndexScore,
    handleSubmit,
    handleEdit,
    handleChange,
    handleDelete,
    handleEditInForm,
    handleSubmitForm,handleEdit15,
    handleSubmitForm15,handleSucces
 }
 return (
     <MahasiswaContext.Provider value={{state,handleFunction}}>

{props.children}
     </MahasiswaContext.Provider>
 )
}

