import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import { MahasiswaContext } from "../context/MahasiswaContext";


const Tugas15Form= ()=> {
        let {slug} = useParams()
       let history = useHistory()
    const {state,handleFunction} = useContext(MahasiswaContext)
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state
    const {
        handleIndexScore,
        handleSubmit,
        handleEdit,
        handleChange,
        handleDelete,
        handleSubmitForm15
      
      } = handleFunction
      useEffect(()=>{

        if(slug !== undefined ){
            axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${slug}`)
        .then((res)=>{
           let data = res.data

            setInput({
                name:data.name,
                course:data.course,
                score:data.score
              }

            )
            setCurrentId(data.id)

           
        })

        .catch((err)=>{
            console.log(err)
        })
        }
        return ()=>{
            setInput({
                name:'',
                course:'',
                score:0
              }

            )
           
        }
        
      },[])
     
     
      
      

    return(

        <>
        
        
        
        <div className="container-form">
        <h1>Form  Nilai Mahasiswa</h1>
            
  <form onSubmit={handleSubmitForm15}>
    <label >Nama :</label>
    <input onChange={handleChange} value={input.name} type="text"  name="name" placeholder="Nama"required/>
    <label >Mata Kuliah :</label>
    <input onChange={handleChange} value={input.course} type="text"  name="course" placeholder="Mata Kuliah" required/>
    <label >Nilai : </label>
    <input onChange={handleChange} value={input.score} type="number"  name="score" min={0} max={100}  placeholder="Nilai"  required/>
    

    
  
    <input type="submit" value="Submit"/>
    <Link to='/tugas15' >Kembali ke tabels</Link>
  </form>
</div>
        </>
    )

}
export default Tugas15Form