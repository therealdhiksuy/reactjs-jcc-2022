import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ButtonSwitchColor from "../component/ButtonSwitchColor";
import { MahasiswaContext } from "../context/MahasiswaContext";


const Tugas15List= ()=> {
    const {state,handleFunction} = useContext(MahasiswaContext)
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state

    const {
      handleIndexScore,
      handleSubmit,
      handleEdit,
      handleChange,
      handleDelete,handleEdit15,handleSucces
    } = handleFunction

      useEffect(()=>{

        let fetchData = async () => {
            let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
        let result = data.map((res)=>{
            let{id, 
                name , 
                course, 
                score, } = res
            return {
                id, 
                name , 
                course, 
                score, }
        })
        
          setDataMahasiswa([...result])
        console.log(data)
           
        }
        if (fetchStatus){
            fetchData()
            setFetchStatus(false)

        }
        
      },[fetchStatus,setFetchStatus])
      
     
      
  

    return(

        <>
   <div className="mt-20 min-g-screen flex items-center justify-center">
  <Link to='/tugas15/create'><button type="button" className="py-2 px-4 m-10  w-96 bg-cyan-500 hover:bg-cyan-600">
    Create Data
</button>
</Link>
</div>
        {/* <div className="">
            <h1>Daftar Nilai Mahasiswa</h1>
        <table id="">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Mata Kuliah</th>
    <th>Nilai</th>
    <th>Indeks Nilai</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
 
      {
      dataMahasiswa.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.name}</td>
    <td>{res.course}</td>
    <td>{res.score}</td>
    <td>{handleIndexScore(res.score)}</td>
  
    <td> 
      <div className="mt-20 min-g-screen flex items-center justify-center" >
        <button onClick={handleEdit} value={res.id}>Edit</button>
        <button onClick={handleDelete} value={res.id}>Delete</button>
        </div>
    </td>
    
  </tr>
           
          )
      })
  }
 
  </tbody>

</table> */}


        {/* </div> */}
      <div className="mt-20 min-g-screen flex items-center justify-center">
        <table className="table p-4 bg-white shadow rounded-lg">
        <thead>
          <tr>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
             No
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Nama
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Mata Kuliah
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Nilai
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Indeks Nilai
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Aksi
            </th>
          </tr>
        </thead>
        <tbody>
        {
      dataMahasiswa.map((res,index)=>{
          return (

          <tr className="text-gray-700" key={index}>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {index +1}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.name}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.course}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.score}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handleIndexScore(res.score)}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            <button  className="py-2 px-4  bg-yellow-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleEdit15} value={res.id}>Edit</button>
            <button className="py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleDelete} value={res.id}>Delete</button>
            
            </td>
          </tr>
           )
          })
      }
        </tbody></table>
        
        </div>
        
        </>
    )

}
export default Tugas15List