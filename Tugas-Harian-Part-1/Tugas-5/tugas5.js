// Soal 1 
{ let word = 'JavaScript'; 
let second = 'is'; 
let third = 'awesome'; 
let fourth = 'and'; 
let fifth = 'I'; 
let sixth = 'love'; 
let seventh = 'it!';
const theString = `${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh} ` 
console.log("//Soal 1")
console.log (theString)
} 
// Soal 2 
{
    let kataPertama = "saya"; 
    let kataKedua = "senang"; 
    let kataKetiga = "belajar"; 
    let kataKeempat = "javascript";
    let empatup = kataKeempat.toUpperCase() ;
    let capfirst = kataKedua.charAt(0).toUpperCase()+kataKedua.substr(1);
    let capend = kataKetiga.substr(0,6)+kataKetiga.charAt(6).toUpperCase();
       
    const theString = `${kataPertama} ${capfirst} ${capend} ${empatup} ` 
    console.log("//Soal 2")
    console.log(theString)

}

// Soal no 3
let panjangPersegiPanjang = "8";
let lebarPersegiPanjang = "5";
let panjangPersegiPanjang_int = parseInt(panjangPersegiPanjang);
let lebarPersegiPanjang_int = parseInt(lebarPersegiPanjang);

let alasSegitiga= "6";
let tinggiSegitiga = "7";

let alasSegitigaint=parseInt(alasSegitiga);
let tinggiSegitigaint=parseInt(tinggiSegitiga);

let kelilingPersegiPanjang = "Keliling Persegi Panjang = " + 2 * (panjangPersegiPanjang_int + lebarPersegiPanjang_int) ;
let luasSegitiga ="Luas Segitiga = " +1/2 * alasSegitigaint * tinggiSegitigaint ;

const skrtt = `${luasSegitiga} ${kelilingPersegiPanjang} `;
console.log("//Soal 3")
console.log(kelilingPersegiPanjang)
console.log(luasSegitiga)


// Soal 4
let sentences= 'wah javascript itu keren sekali'; 

let firstWords= sentences.substring(0, 3); 
let secondWords= sentences.substring(4,14); // do your own! 
let thirdWords= sentences.substring(15,18); // do your own! 
let fourthWords= sentences.substring(18,24); // do your own! 
let fifthWords= sentences.substring(25,31); // do your own! 
console.log("//Soal 4")
console.log('Kata Pertama: ' + firstWords); 
console.log('Kata Kedua: ' + secondWords); 
console.log('Kata Ketiga: ' + thirdWords); 
console.log('Kata Keempat: ' + fourthWords); 
console.log('Kata Kelima: ' + fifthWords);

// Soal 5 
var sentence = "I am going to be React JS Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord=sentence[5] + sentence[6] + sentence[7]+ sentence[8] + sentence[9]; // lakukan sendiri, wajib mengikuti seperti contoh diatas 
var fourthWord=sentence[11]+sentence[12]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var fifthWord=sentence[14]+sentence[15]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var sixthWord=sentence[17]+sentence[18]+sentence[19]+sentence[20]+sentence[21]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var seventhWord=sentence[23]+sentence[24]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var eighthWord=sentence[26]+sentence[27]+sentence[28]+sentence[29]+sentence[30]+sentence[31]+sentence[32]+sentence[33]+sentence[34]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
console.log("//Soal 5")
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

// Soal 6

let txt = "I can eat bananas all day";
let hasil= txt.slice(10,18); 
console.log("//Soal 6")
console.log(hasil)