// Soal 1
console.log("Jawaban soal 1\n")

var nilaiJohn = 80;
var nilaiDoe = 50;


console.log("Indeks Nilai John")

if ( nilaiJohn >=80 ) {
    console.log("A")
} else if ((nilaiJohn >= 70)  && (nilaiJohn <80 ))  {
    console.log("B") 
} else if ((nilaiJohn >= 60)  && (nilaiJohn <70 )) {
    console.log("C")
} else if ((nilaiJohn >= 50)  && (nilaiJohn <70 ))
 {
    console.log("D")
}
else if (nilaiJohn<50  )
 {
    console.log("E")
}
else {
    console.log('error')
}

console.log("Indeks Nilai Doe")

if ( nilaiDoe >=80 ) {
    console.log("A")
} else if ((nilaiDoe >= 70)  && (nilaiDoe <80 ))  {
    console.log("B") 
} else if ((nilaiDoe >= 60)  && (nilaiDoe <70 )) {
    console.log("C")
} else if ((nilaiDoe >= 50)  && (nilaiDoe <70 ))
 {
    console.log("D")
}
else if (nilaiDoe<50  )
 {
    console.log("E")
}
else {
    console.log('error')
}

console.log('\n')


//Soal 2
console.log("Jawaban soal 2\n")
var tanggal = 30;
var bulan = 3;
var tahun = 2000;

switch(bulan){
    case 1 : {console.log('30 Januari 2000');break;}
    case 2 : {console.log('30 Februari 2000');break;}
    case 3 : {console.log('30 Maret 2000');break;}
}
console.log('\n')
// Soal 3
console.log("Jawaban soal 3\n")
console.log('Looping Pertama');
for(var deret = 2; deret <= 20; deret += 2) {
    console.log(deret + ' - I love coding ' );
  }
  
  console.log('Looping Kedua');
   
  for(var deret = 20; deret >=2; deret -= 2) {
    console.log(deret + ' - I will become a frontend developer ');
  } 

// Soal 4
console.log('\n')
console.log("Jawaban soal 4\n")
var a = ''
for (var i=1; i<=20 ;i++){
    if(i%3===0 && i%2!==0){
        a += i + ' - I Love Coding \n'
    }else{
        if(i%2===0){
            a += i + ' - Berkualitas \n'
        }else{
            a += i + ' - Santai \n'
        }
    } 
    
}
console.log(a)

// Soal 5
console.log("Jawaban soal 5\n")
var d =''
for (var i=0; i<7;i++){
    for (var j=0;j<=i;j++){
d+= '#'
    }
d+='\n'
}
console.log(d)