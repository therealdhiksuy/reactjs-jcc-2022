import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { MobileContext } from "../context/MobileContext";



const MobileList= ()=> {
    const {state,handleFunction} = useContext(MobileContext)
    const {
      dataMobile,setdataMobile,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state

    const {
      handleIndexScore,
      handleSubmit,
      handleEdit,
      handleChange,
      handleDelete,handleEdit15,handleSucces,handleAndroid,handleIos,handlePrice,handleSize
    } = handleFunction

      useEffect(()=>{

        let fetchData = async () => {
            let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps`)
        let result = data.map((res)=>{
            let{category,
                description,
                id,
                image_url,
                name,
                price,
                rating,
                release_year,
                size,
                is_android_app,
                is_ios_app



     } = res
            return {
                category,
                description,
                id,
                image_url,
                name,
                price,
                rating,
                release_year,
                size,is_android_app,
                is_ios_app }
        })
        
          setdataMobile([...result])
          console.log(result)  
           
        }
        if (fetchStatus){
            fetchData()
            setFetchStatus(false)

        }
        
      },[fetchStatus,setFetchStatus])
      
     
    
  

    return(

        <>
   <div className="mt-20 min-g-screen flex items-center justify-center">
  <Link to='/mobileform'><button type="button" className="py-2 px-4 m-10  w-96 bg-cyan-500 hover:bg-cyan-600">
    Create Data
</button>
</Link>
</div>
        {/* <div className="">
            <h1>Daftar Nilai Mahasiswa</h1>
        <table id="">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Mata Kuliah</th>
    <th>Nilai</th>
    <th>Indeks Nilai</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
 
      {
      dataMahasiswa.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.name}</td>
    <td>{res.course}</td>
    <td>{res.score}</td>
    <td>{handleIndexScore(res.score)}</td>
  
    <td> 
      <div className="mt-20 min-g-screen flex items-center justify-center" >
        <button onClick={handleEdit} value={res.id}>Edit</button>
        <button onClick={handleDelete} value={res.id}>Delete</button>
        </div>
    </td>
    
  </tr>
           
          )
      })
  }
 
  </tbody>

</table> */}


        {/* </div> */}
      <div className=" min-g-screen flex items-center justify-center">
        <table className="m-20 table p-4 bg-white shadow rounded-lg">
        <thead>
          <tr>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
             No
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Nama
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Category
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Description
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Release Year
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Size
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Price
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Rating
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Platform
            </th>
           
            
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
        {
      dataMobile.map((res,index)=>{
          return (

          <tr className="text-gray-700" key={index}>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {index +1}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.name}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.category}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.description}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.release_year}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handleSize(res.size)}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handlePrice(res.price)}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.rating}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handleAndroid(res.is_android_app)}<br/>
            {handleIos(res.is_ios_app)}
            </td>
            
            <td className="border-b-2 p-4 dark:border-dark-5">
            <button  className="m-2 py-2 px-4  bg-yellow-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleEdit15} value={res.id}>Edit</button>
            <button className="m-2  py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleDelete} value={res.id}>Delete</button>
            
            </td>
          </tr>
           )
          })
      }
        </tbody></table>
        
        </div>
        
        </>
    )

}
export default MobileList