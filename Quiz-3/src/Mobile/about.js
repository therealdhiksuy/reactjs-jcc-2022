import React from "react";

const About = () => {
    return (

        <>
         {/* <div style={{borderStyle: 'groove'}}>
          <h1 style={{textAlign: 'center'}}>Data Peserta Jabarcodingcamp Reactjs</h1>
          <h3 />
          <p> 1.&nbsp;Nama : Priandhika Lubis</p>
          <p> 2.&nbsp;Email : therealdhiksuy@gmail.com</p>
          <p> 3.&nbsp;Sistem Operasi : Windows 10</p>
          <p> 4.&nbsp;Akun gitlab : @therealdhiksuy</p>
          <p> 5.&nbsp;Akun telegram : @fclsskrt</p>
        </div>
        <br />
         */}

<div className="bg-white dark:bg-gray-800">
        <div className="relative px-4 py-6 overflow-hidden sm:px-6 sm:py-8 lg:p-12 xl:p-16">
          <h2 className="text-2xl font-semibold font-display text-black dark:text-white sm:text-3xl">
          Data Peserta Jabarcodingcamp Reactjs
          </h2>
          
          <p className="mt-2 max-w-xl text-base text-gray-400"> 1.&nbsp;Nama : Priandhika Lubis</p>
          <p className="mt-2 max-w-xl text-base text-gray-400"> 2.&nbsp;Email : therealdhiksuy@gmail.com</p>
          <p className="mt-2 max-w-xl text-base text-gray-400"> 3.&nbsp;Sistem Operasi : Windows 10</p>
          <p className="mt-2 max-w-xl text-base text-gray-400"> 4.&nbsp;Akun gitlab : @therealdhiksuy</p>
          <p className="mt-2 max-w-xl text-base text-gray-400"> 5.&nbsp;Akun telegram : @fclsskrt</p>
         
          <div className="hidden lg:block absolute inset-y-0 lg:left-2/3 xl:left-1/2 right-0">
            
          </div>
        </div>
      </div>
      </>
    )
}

export default About