import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";

import { MobileContext } from "../context/MobileContext";


const MobileForm= ()=> {
    
        let {slug} = useParams()
       let history = useHistory()
    const {state,handleFunction} = useContext(MobileContext)
    const [checked, setChecked] = useState(false);
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state
    const {
        handleIndexScore,
        handleSubmit,
        handleEdit,
        handleChange,
        handleDelete,
        handleSubmitForm15
      
      } = handleFunction
      useEffect(()=>{

        if(slug !== undefined ){
            axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${slug}`)
        .then((res)=>{
           let data = res.data
                console.log(data)
            setInput({
                category:data.category,
                description:data.description ,
                image_url:data.image_url,
                name:data.name,
                price:data.price,
                rating:data.rating,
                release_year : data.release_year,
                size:data.size,
                is_android_app:data.is_android_app,
                is_ios_app:data.is_ios_app

              }

            )
            setCurrentId(data.id)

           
        })

        .catch((err)=>{
            console.log(err)
        })
        }
        return ()=>{
            setInput({
                category:"",
                description:"",
                image_url:"",
                name:"",
                price:"",
                rating:"",
                release_year : 2007,
                size:""
              }

            )
           
        }
        
      },[])
     
     
      
      

    return(

        <>
        
        
        
        <div className="container-form">

            

    {/* <label >Nama :</label>
    <input onChange={handleChange} value={input.name} type="text"  name="name" placeholder="Nama"required/>
    <label >Mata Kuliah :</label>
    <input onChange={handleChange} value={input.course} type="text"  name="course" placeholder="Mata Kuliah" required/>
    <label >Nilai : </label>
    <input onChange={handleChange} value={input.score} type="number"  name="score" min={0} max={100}  placeholder="Nilai"  required/> */}
      <form onSubmit={handleSubmitForm15}>
<div className="m-20 bg-white rounded-lg shadow sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden">
        <div className="px-4 py-8 sm:px-10">
          <div className="relative mt-6">
            <div className="absolute inset-0 flex items-center">
              <div className="w-full border-t border-gray-300">
              </div>
            </div>
            <div className="relative flex justify-center text-sm leading-5">
              <span className="px-2 text-gray-500 bg-white">
                Form Mobile
              </span>
            </div>
          </div>
          <div className="mt-6">
            <div className="w-full space-y-6">
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               Name
              </span>
                  <input onChange={handleChange} value={input.name} type="text"  name="name" id="search-form-price" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" min ={0} placeholder="Name" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Category
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.category} type="text"  name="category" id="search-form-location" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Category" required/>
                </div>
              </div>
              <span className="px-2 text-gray-500 bg-white">
               Description
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.description} type="text"  name="description" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Description" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Years Release
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.release_year} type="number"  name="release_year" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent " min={2007} max={2021} placeholder="Release Year" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Size
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.size} type="number"  name="size" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Size"required />
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Price
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.price} type="number"  name="price" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" min={0} placeholder="Price" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Rating
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.rating} type="number"  name="rating" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" min={0} max={5} placeholder="Rating"  required/>
                </div>
              </div>
              {/* <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
  <label for="vehicle1"> I have a bike</label><br>
  <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">
  <label for="vehicle2"> I have a car</label><br> */}
   <div> <span className="mt-5 px-2 text-gray-500 bg-white">
               Platform
              </span>
        <input type="checkbox" id="vehicle1" name="is_android_app" value='true' />
        <label htmlFor="vehicle1"> Android</label><br />
        <input type="checkbox" id="vehicle2" name="is_ios_app" value='true' />
        <label htmlFor="vehicle2"> IOS</label><br />
      </div>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.image_url} type="text"  name="image_url" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Image_Url"required />
                </div>
              </div>
              <div>
                <span className="block w-full rounded-md shadow-sm">
                
                  
<button type="submit" value="submit" className="py-2 px-4 flex justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
    <svg width="20" height="20" className="mr-2" fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
        <path d="M1344 1472q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm256 0q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm128-224v320q0 40-28 68t-68 28h-1472q-40 0-68-28t-28-68v-320q0-40 28-68t68-28h427q21 56 70.5 92t110.5 36h256q61 0 110.5-36t70.5-92h427q40 0 68 28t28 68zm-325-648q-17 40-59 40h-256v448q0 26-19 45t-45 19h-256q-26 0-45-19t-19-45v-448h-256q-42 0-59-40-17-39 14-69l448-448q18-19 45-19t45 19l448 448q31 30 14 69z">
        </path>
    </svg>
    Submit
</button>
<button type="button" className="mt-5 py-2 px-4 flex justify-center items-center  bg-red-500 hover:bg-red-700 focus:ring-red-500 focus:ring-offset-red-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"><Link to='/mobilelist' >Kembali ke tabels</Link></button>

                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="px-4 py-6 border-t-2 border-gray-200 bg-gray-50 sm:px-10">
          <p className="text-xs leading-5 text-gray-500">
           Form Mobile
          </p>
        </div>
      </div>
    

    
  
 
  </form>
</div>
        </>
    )

}
export default MobileForm