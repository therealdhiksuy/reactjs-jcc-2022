
import '../css/style.css'
import logo from '../img/logo.png'
import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { MobileContext } from "../context/MobileContext";
const Home = ()=> {
  const {state,handleFunction} = useContext(MobileContext)
  const {
    dataMobile,setdataMobile,
    currentId, setCurrentId,
    fetchStatus ,setFetchStatus,
    input, setInput
  } = state

  const {
    handleIndexScore,
    handleSubmit,
    handleEdit,
    handleChange,
    handleDelete,handleEdit15,handleSucces,handleAndroid,handleIos,handlePrice,handleSize
  } = handleFunction

    useEffect(()=>{

      let fetchData = async () => {
          let {data} = await axios.get(`https://backendexample.sanbercloud.com/api/mobile-apps`)
      let result = data.map((res)=>{
          let{category,
              description,
              id,
              image_url,
              name,
              price,
              rating,
              release_year,
              size,
              is_android_app,
              is_ios_app



   } = res
          return {
              category,
              description,
              id,
              image_url,
              name,
              price,
              rating,
              release_year,
              size,is_android_app,
              is_ios_app }
      })
      
        setdataMobile([...result])
        console.log(result)  
         
      }
      if (fetchStatus){
          fetchData()
          setFetchStatus(false)

      }
      
    },[fetchStatus,setFetchStatus])
    
   
  


  return(

      <>
 
      {/* <div className="">
          <h1>Daftar Nilai Mahasiswa</h1>
      <table id="">
<thead>

<tr>
  <th>No</th>
  <th>Nama</th>
  <th>Mata Kuliah</th>
  <th>Nilai</th>
  <th>Indeks Nilai</th>
  <th>Aksi</th>
</tr>

</thead>
<tbody>


    {
    dataMahasiswa.map((res,index)=>{
        return (

        
          <tr key={index}>
  <td>{index +1}</td>
  <td>{res.name}</td>
  <td>{res.course}</td>
  <td>{res.score}</td>
  <td>{handleIndexScore(res.score)}</td>

  <td> 
    <div className="mt-20 min-g-screen flex items-center justify-center" >
      <button onClick={handleEdit} value={res.id}>Edit</button>
      <button onClick={handleDelete} value={res.id}>Delete</button>
      </div>
  </td>
  
</tr>
         
        )
    })
}

</tbody>

</table> */}


      {/* </div> */}
    
      {
    dataMobile.map((res,index)=>{
        return (

          <div className="bg-indigo-900 relative overflow-hidden h-screen">
        <img src={res.image_url} className="absolute h-full w-full object-cover" />
        <div className="inset-0 bg-black opacity-25 absolute">
        </div>
        <header className="absolute top-0 left-0 right-0 z-20">
          <nav className="container mx-auto px-6 md:px-12 py-4">
            <div className="md:flex justify-between items-center">
              <div className="flex justify-between items-center">
                <a href="#" className="text-white">
                  
                </a>
                <div className="md:hidden">
                  <button className="text-white focus:outline-none">
                    <svg className="h-12 w-12" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M4 6H20M4 12H20M4 18H20" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round">
                      </path>
                    </svg>
                  </button>
                </div>
              </div>
              
            </div>
          </nav>
        </header>
        <div className="container mx-auto px-6 md:px-12 relative z-10 flex items-center py-32 xl:py-40">
          <div className="lg:w-3/5 xl:w-2/5 flex flex-col items-start relative z-10">
            <span className="font-bold uppercase text-yellow-400">
              release year : {res.release_year}
            </span>
            <h1 className="font-bold text-6xl sm:text-7xl text-white leading-tight mt-4">
              {res.name}
              <br />
              
            </h1>
            
            <br/>
            <span className="border-0 font-black uppercase text-white">
              Price : {handlePrice(res.price)}
            </span>
            <span className="font-black uppercase text-white">
              Rating : {res.rating}
            </span>
            <span className="font-black uppercase text-white">
              Size : {handleSize(res.size)}
            </span>
            <span className="font-black uppercase text-white">
              Platform : {handleIos(res.is_ios_app)} <br/><span>{handleAndroid(res.is_android_app)}</span>
            </span>
            <span className="font-black uppercase text-white">
              Description : {res.description}</span>
           
          </div>
        </div>
      </div>
         )
        })
    }
      
      
      </>
  )

    // return(
    //     <>
        
    //     <div>
        
    //     <div className="row">
    //       <div className="section">
    //         <div className="card">
    //           <div>
    //             <h2>bame</h2>
    //             <h5>Release Year : year</h5>
    //             <img className="fakeimg" style={{width: '50%', height: '300px', objectFit: 'cover'}} src />
    //             <br />
    //             <br />
    //             <div>
    //               <strong>Price: price</strong><br />
    //               <strong>Rating: rating</strong><br />
    //               <strong>Size: size</strong><br />
    //               <strong style={{marginRight: '10px'}}>Platform : Android &amp; IOS
    //               </strong>
    //               <br />
    //             </div>
    //             <p>
    //               <strong style={{marginRight: '10px'}}>Description :</strong>
    //               {'{'}item.description{'}'}
    //             </p>
    //             <hr />
    //           </div>
    //         </div>
    //       </div>
    //     </div>
        
    //   </div>
    //     </>
    // // )

}

export default Home