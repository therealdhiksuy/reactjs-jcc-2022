import React from "react";

import { BrowserRouter as Router , Route ,Switch } from "react-router-dom";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";
import SearchBar from "../component/SearchBar";
import { MobileProvider } from "../context/MobileContext";
import Home from "../Home/home";
import About from "../Mobile/about";
import MobileForm from "../Mobile/MobileForm";
import MobileList from "../Mobile/MobileList";


const Routes = () => {

    return (

        <>
        <Router>
            <Navbar/>
            <MobileProvider> 
        <Switch>


        <Route path={'/'} exact component ={Home} />
        <Route path={'/about'} exact component ={About} />
        <Route path={'/mobilelist'} exact component ={MobileList} />
        <Route path={'/mobileform'} exact component ={MobileForm} />
        <Route path={'/mobileform/edit/:slug'} exact component ={MobileForm} />

        </Switch>
        </MobileProvider>



        </Router>
        <Footer/>
        </>
    )
}

export default Routes 