import React, { createContext, useState } from "react";
import axios from "axios"
import { useHistory } from "react-router-dom";


export const MobileContext = createContext()
export const MobileProvider = props =>{
    let history = useHistory()
    let [dataMobile,setdataMobile] = useState([]) 
    const [currentId, setCurrentId] = useState(-1)
    const [fetchStatus ,setFetchStatus] = useState(true)
    const [input, setInput] = useState({
      category:'',
      description:'',
      image_url:'',
      name:'',
      price:0,
      rating:'',
      release_year : 2007,
      size:'',
      is_android_app:true,
                is_ios_app:true
    })
    const handleDelete = (event) => {
        let dataId = parseInt(event.target.value)
       
        axios.delete(`https://backendexample.sanbercloud.com/api/mobile-apps/${dataId}`)
        .then((res)=>{
          
            // console.log(res)
            setFetchStatus(true)
        })

        .catch((err)=>{
            console.log(err)
        })

       
       
       

      }
      const handleEdit = (event) => {
        let dataId = parseInt(event.target.value)
       history.push(`/tugas14/edit/${dataId}`)
        
        // let editValue = dataMobile[dataId]
        // setInput(editValue)
        // setCurrentId(event.target.value)

        
        
       
      }
      const handleEdit15 = (event) => {
        let dataId = parseInt(event.target.value)
       history.push(`/mobileform/edit/${dataId}`)
        
        // let editValue = dataMobile[dataId]
        // setInput(editValue)
        // setCurrentId(event.target.value)

        
        
       
      }
      const handleEditInForm = (event) => {

        let dataId = parseInt(event.target.value)
        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${dataId}`)
        .then((res)=>{
           let data = res.data

            setInput({
                name:data.name,
                course:data.course,
                score:data.score
              }

            )
            setCurrentId(data.id)

           
        })

        .catch((err)=>{
            console.log(err)
        })
        }
        


      
      const handleChange =(event)=>{
        let name = event.target.name 
        let value = event.target.value
        
  setInput({...input,[name]:value})
        

      }
      const handleSubmit = (event) => {
          event.preventDefault()
          
          let {name,course,score} = input
          let data = dataMobile

        //   let data = dataBuah
          
          if(currentId===-1){
              axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name,course,score})

              .then((res)=>{
                history.push('/tugas14')
               setFetchStatus(true)
              })

            // data = [...data,{nama , hargaTotal , beratTotal}]
            
          }else {
            axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,input)
            .then((res)=>{
              history.push('/tugas14')
              
                setFetchStatus(true)
               })
          }
          setInput ({
            name:"",
            course:"",
            score:0
          })
          setCurrentId(-1)
    
        //   setDataBuah([...data])
      }
      const handleSucces = () => {
        return (
          <>
          
          </>
        )
      }
      const handleSubmitForm = (event) => {
        event.preventDefault()
        
        let {name,course,score} = input
        let data = dataMobile

      //   let data = dataBuah
        
        if(currentId===-1){
            axios.post(`https://backendexample.sanbercloud.com/api/student-scores`,{name,course,score})

            .then((res)=>{
              
             setFetchStatus(true)
            })

          // data = [...data,{nama , hargaTotal , beratTotal}]
          
        }else {
          axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,input)
          .then((res)=>{
           
            
              setFetchStatus(true)
             })
        }
        setInput ({
          name:"",
          course:"",
          score:0
        })
        setCurrentId(-1)
  
      //   setDataBuah([...data])
    }
    const handleSubmitForm15 = (event) => {
      event.preventDefault()
      
      let {name,category,description,release_year,size,price,rating,is_android_app,is_ios_app,image_url} = input
      let data = dataMobile

    //   let data = dataBuah
      
      if(currentId===-1){
          axios.post(`https://backendexample.sanbercloud.com/api/mobile-apps`,input)

          .then((res)=>{
           
            history.push('/mobilelist')
            
           setFetchStatus(true)
          })

        // data = [...data,{nama , hargaTotal , beratTotal}]
        
      }else {
        axios.put(`https://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`,input)
        .then((res)=>{
          history.push('/mobilelist')
         
            setFetchStatus(true)
           })
      }
      setInput ({
        name:"",
        course:"",
        score:0
      })
      setCurrentId(-1)

    //   setDataBuah([...data])
  }
      
    
      const handleIndexScore = (param)=>{
        if(param>=80){
            return "A"
        }
        else if (param>=70 && param<80 ){
            return "B"
        }
        else if(param>=60 && param <70){
            return "C"
        }
        else if (param>=50 && param <60){
            return "D"
        }
        else {
            return "E"
        }

        
  }  
 const handleIos= (param) => {
  if(param===1){
    return "IOS "
}
else{
    return " "
}

 }
 const handleAndroid= (param) => {
  if(param===1){
    return "Android "
}
else{
    return " "
}
 }
 const handlePrice = (param) => {
if (param===0){
  return "Free"
  
} else {
  return param
}
 }

 const handleSize = (param) => {

  if (param>=1000){
    return param/1000 + "GB"
  }
  else{
    return param+ "MB"
  }

 }
//  handleBoolean = (param) =>
//  {
//    if(param===true) {
//     checked={checked}
//    }
//  }

 let state = {
     dataMobile,setdataMobile,
     currentId, setCurrentId,
     fetchStatus ,setFetchStatus,
     input, setInput

 }
 let handleFunction ={
    handleIndexScore,
    handleSubmit,
    handleEdit,
    handleChange,
    handleDelete,
    handleEditInForm,
    handleSubmitForm,handleEdit15,
    handleSubmitForm15,handleSucces,handleAndroid,handleIos,handlePrice,handleSize
 }
 return (
     <MobileContext.Provider value={{state,handleFunction}}>

{props.children}
     </MobileContext.Provider>
 )
}

