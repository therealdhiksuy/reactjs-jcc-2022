import axios from "axios";
import Cookies from "js-cookie";
import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import styledComponents from "styled-components";

const Section = styledComponents.section`
height:85vh;
background: rgb(156,47,47);
background: linear-gradient(90deg, rgba(156,47,47,1) 9%, rgba(110,60,60,1) 100%);


`

const Register = () => {

  
    
   
    
    let history = useHistory()
      const [inputLogin, setInputLogin] = useState({
          name:"",
          image_url:"",
        email:"",
        password:"",
        
    })
      
    const handleChange = (event) => {
    
      let name = event.target.name
      let value= event.target.value
     
      setInputLogin ({...inputLogin,[name]:value}) 

    
    }
      
     const handleRegister = (event) => {
        event.preventDefault()
        
        
            axios.post('https://dev-example.sanbercloud.com/api/register ',inputLogin)
        .then((res)=>{
          console.log(res)
         
          window.location = "/login"
        })
        .catch((err)=>{
          alert(err)
        })
        
        
       
    
        
    
      }


    return (


        <>
        <Section>
        

            

{/* <label >Nama :</label>
<input onChange={handleChange} value={input.name} type="text"  name="name" placeholder="Nama"required/>
<label >Mata Kuliah :</label>
<input onChange={handleChange} value={input.course} type="text"  name="course" placeholder="Mata Kuliah" required/>
<label >Nilai : </label>
<input onChange={handleChange} value={input.score} type="number"  name="score" min={0} max={100}  placeholder="Nilai"  required/> */}

  <form onSubmit={handleRegister}>
  <div className="flex items-stretch">
    <div className="relative m-10 bg-white rounded-lg shadow sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden ring-offset-2 ring">
    <div className="px-4 py-8 sm:px-10">
      <div className="relative mt-6">
        <div className="absolute inset-0 flex items-center">
          <div className="w-full border-t border-gray-300">
          </div>
        </div>
        <div className="relative flex justify-center text-sm leading-5">
          <span className="px-2 text-gray-500 bg-white">
            Form Register
          </span>
        </div>
      </div>
      <div className="mt-6">
        <div className="w-full space-y-6">
        <div className="w-full">
            <div className=" relative ">
            <span className="px-2 text-gray-500 bg-white">
           Name
          </span>
              <input onChange={handleChange} value={inputLogin.name} type="text"  name="name"  className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="Name" required/>
            </div>
          </div>
          <div className="w-full">
            <div className=" relative ">
            <span className="px-2 text-gray-500 bg-white">
           Image Url
          </span>
              <textarea onChange={handleChange} value={inputLogin.image_url} type="text"  name="image_url"  className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="Image Url" required/>
            </div>
          </div>
          <div className="w-full">
            <div className=" relative ">
            <span className="px-2 text-gray-500 bg-white">
           Email
          </span>
              <input onChange={handleChange} value={inputLogin.email}  type="text"  name="email" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="Email" required/>
            </div>
          </div>
          <div className="w-full">
            <div className=" relative ">
            <span className="px-2 text-gray-500 bg-white">
           Password
          </span>
          <input  onChange={handleChange} value={inputLogin.password} type="password" id="password" name="password" minLength={8} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="Password"  required/>
          
            </div>
          </div>
          <div>
            <span className="block w-full rounded-md shadow-sm">
            
              
<button type="submit" value="submit" className="py-2 px-4 flex justify-center items-center  bg-blue-500 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">

Register
</button>


            </span>
          </div>
        </div>
      </div>
    </div>
    <div className="px-4 py-6 border-t-2 border-gray-200 bg-gray-50 sm:px-10">
      <p className="text-xs leading-5 text-gray-500">
       Form Register
      </p>
    </div>
  </div>




</div>
</form>

</Section>
        </>
    )


}

export default Register
