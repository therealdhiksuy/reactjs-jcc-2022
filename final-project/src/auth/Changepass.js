import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import styledComponents from "styled-components";
import Sidebar from "../component/Sidebar";


const Section = styledComponents.section`
height:85vh;
background: rgb(156,47,47);
background: linear-gradient(90deg, rgba(156,47,47,1) 9%, rgba(110,60,60,1) 100%);


`


const Changepass = () => {

    let history = useHistory()
    const [currentId, setCurrentId] = useState(-1)
    const [fetchStatus ,setFetchStatus] = useState(true)
      const [inputLogin, setInputLogin] = useState({
        current_password:"",
        new_password:"" ,
        new_confirm_password:""
        
        
    })
      
    const handleChange = (event) => {
    
      let name = event.target.name
      let value= event.target.value
      setInputLogin ({...inputLogin,[name]:value}) 
    
    
    }
      
     const handleChangePassword = (event) => {
        event.preventDefault()
        let {current_password,
        new_password,
        new_confirm_password} = inputLogin
            
        if (new_password === new_confirm_password) {
            axios.post('https://dev-example.sanbercloud.com/api/change-password ',inputLogin,{
            headers : {
              "Authorization" : "Bearer" + Cookies.get('token')
            }
          })
        .then(()=>{
     
            history.push('/profile')
            alert('Password Telah Diubah')
         
            setFetchStatus(true)
        })
        .catch((err)=>{
          alert(err)
        })
        console.log(inputLogin)
    
    
        }
        else {
            alert('Password Baru dan Confirm Password Baru harus sama')
        }
        
    
      }
    
        return (
    
    
            <>
          
            <Section>
            <div className="container-form">
    
                
    

      <form onSubmit={handleChangePassword}>
          <div className="flex items-stretch">
    <div className="relative m-20 bg-white rounded-lg shadow sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden ring-offset-2 ring">
        <div className="px-4 py-8 sm:px-10 mx-8">
          <div className="relative mt-6">
            <div className="absolute inset-0 flex items-center">
              <div className="w-full border-t border-gray-300 " >
              </div>
            </div>
            <div className="relative flex justify-center text-sm leading-5">
              <span className="px-2 text-gray-500 bg-white">
                Form Change Password
              </span>
            </div>
          </div>
          <div className="mt-6">
            <div className="w-full space-y-6">
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               Current Password
              </span>
                  <input onChange={handleChange} value={inputLogin.current_password} type="password" id="password" minLength={8} name="current_password" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="" required/>
                </div>
              </div>
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               New Password
              </span>
              <input  onChange={handleChange} value={inputLogin.new_password} type="password" id="password" name="new_password" minLength={8} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder=""  required/>
                </div>
              </div>
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               Confirm New Password
              </span>
              <input  onChange={handleChange} value={inputLogin.new_confirm_password} type="password" id="password" name="new_confirm_password" minLength={8} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder=""  required/>
                </div>
              </div>
             
              <div>
                <span className="block w-full rounded-md shadow-sm">
                
                  
    <button type="submit" value="submit" className="py-2 px-4 flex justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
    
    Change Password
    </button>
    
    
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="px-4 py-6 border-t-2 border-gray-200 bg-gray-50 sm:px-10">
          <p className="text-xs leading-5 text-gray-500">
           Form Change Password
          </p>
        </div>
      </div>
    
    
    
      </div>
    
    </form>
    </div>
    </Section>
            </>
        )
    
    
    }

export default Changepass 