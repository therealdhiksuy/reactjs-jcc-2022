import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import styledComponents from "styled-components";
import Sidebar from "../component/Sidebar";


const Section = styledComponents.section`
height:85vh;
background: rgb(156,47,47);
background: linear-gradient(90deg, rgba(156,47,47,1) 9%, rgba(110,60,60,1) 100%);


`


const Login = () => {

    let history = useHistory()
      const [inputLogin, setInputLogin] = useState({
        email:"",
        password:"" ,
        
        
    })
      
    const handleChange = (event) => {
    
      let name = event.target.name
      let value= event.target.value
      setInputLogin ({...inputLogin,[name]:value}) 
    
    
    }
      
     const handleLogin = (event) => {
        event.preventDefault()
        let {email,password} = inputLogin
    
        axios.post('https://dev-example.sanbercloud.com/api/login ',inputLogin)
        .then(({data})=>{
     
          Cookies.set('token',data.token,{expires:1})
          Cookies.set('user',JSON.stringify(data.user),{expires:1})
          window.location = "/dashboard"
        })
        .catch((err)=>{
          alert(err)
        })
        console.log(inputLogin)
    
    
        
    
      }
    
        return (
    
    
            <>
          
            <Section>
            <div className="container-form">
    
                
    

      <form onSubmit={handleLogin}>
          <div className="flex items-stretch">
    <div className="relative m-20 bg-white rounded-lg shadow sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden ring-offset-2 ring">
        <div className="px-4 py-8 sm:px-10 mx-8">
          <div className="relative mt-6">
            <div className="absolute inset-0 flex items-center">
              <div className="w-full border-t border-gray-300 " >
              </div>
            </div>
            <div className="relative flex justify-center text-sm leading-5">
              <span className="px-2 text-gray-500 bg-white">
                Form Login
              </span>
            </div>
          </div>
          <div className="mt-6">
            <div className="w-full space-y-6">
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               Email
              </span>
                  <input onChange={handleChange} value={inputLogin.email} type="text"  name="email" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="Email" required/>
                </div>
              </div>
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               Password
              </span>
              <input  onChange={handleChange} value={inputLogin.password} type="password" id="password" name="password" minLength={8} className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder="Password"  required/>
                </div>
              </div>
             
              <div>
                <span className="block w-full rounded-md shadow-sm">
                
                  
    <button type="submit" value="submit" className="py-2 px-4 flex justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
    
    Login
    </button>
    
    
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="px-4 py-6 border-t-2 border-gray-200 bg-gray-50 sm:px-10">
          <p className="text-xs leading-5 text-gray-500">
           Form Login
          </p>
        </div>
      </div>
    
    
    
      </div>
    
    </form>
    </div>
    </Section>
            </>
        )
    
    
    }

export default Login 