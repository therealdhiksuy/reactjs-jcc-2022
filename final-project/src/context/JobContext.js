import React, { createContext, useState } from "react";
import axios from "axios"
import { useHistory } from "react-router-dom";
import Cookies from 'js-cookie'


export const JobContext = createContext()
export const JobProvider = props =>{
    let history = useHistory()
    let [dataJob,setdataJob] = useState([]) 
    const [currentId, setCurrentId] = useState(-1)
    const [fetchStatus ,setFetchStatus] = useState(true)
    const [input, setInput] = useState({
        company_city: "",
        company_image_url: "",
        company_name: "",
        job_description: "",
        job_qualification: "",
        job_status: 1,
        job_tenure: "",
        job_type: "",
        salary_max: 0,
        salary_min: 0,
        title: "",
    })
    const handleDelete = (event) => {
        let dataId = parseInt(event.target.value)
       console.log(dataId)
        axios.delete(`https://dev-example.sanbercloud.com/api/job-vacancy/${dataId}`,{
          headers : {
            "Authorization" : "Bearer" + Cookies.get('token')
          }
        })
        .then((res)=>{
          
             console.log(res)
            setFetchStatus(true)
        })

        .catch((err)=>{
            console.log(err)
        })

       
       
       

      }
      const handleEdit = (event) => {
        let dataId = parseInt(event.target.value)
       
       history.push(`/dashboard/list-job-vacancy/form/edit/${dataId}`)
        
        // let editValue = dataMobile[dataId]
        // setInput(editValue)
        // setCurrentId(event.target.value)

        
        
       
      }
      const handleDetail = (event) => {

        let dataId = parseInt(event.target.value)
        history.push(`/list-job-vacancy/detail/${dataId}`)
        // console.log(dataId)
      }
    
  
        


      
      const handleChange =(event)=>{
        let name = event.target.name 
        let value = event.target.value
        let job = ["job_status"] 
        if(job.indexOf(name)===-1){
          setInput({...input,[name]:value})
        }else {
          setInput({...input,[name]:!input[name] })
        }
        
 
        

      }
    
    const handleSubmitForm15 = (event) => {
      event.preventDefault()
      
      let {company_city,
        company_image_url,
        company_name,
        job_description,
        job_qualification,
        job_status,
        job_tenure,
        job_type,
        salary_max,
        salary_min,
        title} = input
      let data = dataJob

    //   let data = dataBuah
      
      if(currentId===-1){
          axios.post(`https://dev-example.sanbercloud.com/api/job-vacancy`,input,{
            headers : {
              "Authorization" : "Bearer" + Cookies.get('token')
            }
          })

          .then((res)=>{
           
            history.push('/dashboard/list-job-vacancy')
            
           setFetchStatus(true)
          })

        // data = [...data,{nama , hargaTotal , beratTotal}]
        
      }else {
        axios.put(`https://dev-example.sanbercloud.com/api/job-vacancy/${currentId}`,input,{
          headers : {
            "Authorization" : "Bearer" + Cookies.get('token')
          }
        })
        .then((res)=>{
          history.push('/dashboard/list-job-vacancy')
         
            setFetchStatus(true)
           })
      }
      setInput ({
        company_city: "",
        company_image_url: "",
        company_name: "",
        job_description: "",
        job_qualification: "",
        job_status: 1,
        job_tenure: "",
        job_type: "",
        salary_max: 0,
        salary_min: 0,
        title: "",
      })
      setCurrentId(-1)

    //   setDataBuah([...data])
  }
  const [search,setSearch] = useState("")
      const handleSearch = (e) =>{
        e.preventDefault()
        setFetchStatus(true)
      
        window.location =`/search/${search}`
      }
    
  
      const handleChangeSearch = (e) => {
        setSearch(e.target.value)
      }

 let state = {
     dataJob,setdataJob,
     currentId, setCurrentId,
     fetchStatus ,setFetchStatus,
     input, setInput,
     search,setSearch


 }
 let handleFunction ={

    handleEdit,
    handleChange,
    handleDelete,
    handleDetail,
    handleSubmitForm15,handleSearch,handleChangeSearch
 }
 return (
     <JobContext.Provider value={{state,handleFunction}}>

{props.children}
     </JobContext.Provider>
 )
}

