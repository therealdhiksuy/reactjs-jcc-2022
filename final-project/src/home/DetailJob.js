import axios from "axios";
import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import styledComponents from "styled-components";
import { JobContext } from "../context/JobContext";
import * as icon from "react-icons/ri";


const Section = styledComponents.div`
height:auto;
background-image: url(https://images.unsplash.com/photo-1531685250784-7569952593d2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
background-repeat: no-repeat;
background-size: cover;
`
const DetailJob = () => {

    let {slug} = useParams()
    let history = useHistory()
 const {state,handleFunction} = useContext(JobContext)

 const {
   
   currentId, setCurrentId,
   fetchStatus ,setFetchStatus,
   input, setInput
 } = state
 const {

     handleChange,

     handleSubmitForm15
   
   } = handleFunction
   useEffect(()=>{

     if(slug !== undefined ){
         axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${slug}`)
     .then((res)=>{
        let data = res.data
             console.log(data)
         setInput({
             company_city:data.company_city,
         company_image_url:data.company_image_url,
         company_name:data.company_name,
         job_description:data.job_description,
         job_qualification:data.job_qualification,
         job_status:data.job_status,
         job_tenure:data.job_tenure,
         job_type:data.job_type,
         salary_max:data.salary_max,
         salary_min:data.salary_min,
         title:data.title

           }

         )
         setCurrentId(data.id)

        
     })

     .catch((err)=>{
         console.log(err)
     })
     }
     return ()=>{
         setInput({
             company_city: "",
             company_image_url: "",
             company_name: "",
             job_description: "",
             job_qualification: "",
             job_status: 1,
             job_tenure: "",
             job_type: "",
             salary_max: 0,
             salary_min: 0,
             title: "",
           }

         )
        
     }
     
   },[fetchStatus,setFetchStatus])
    return (

        <>
        <Section>
          
        <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
        {/*Main Col*/}
        <div id="profile" className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
          <div className="p-4 md:p-12 text-center lg:text-left">
            {/* Image for mobile view*/}
           
            <h1 className="text-3xl font-bold pt-8 lg:pt-0 text-justify">{input.title}</h1>
            <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-green-500 opacity-25" />
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"><icon.RiTimeFill/> 	&nbsp; Job Tenure :	&nbsp; {input.job_tenure}  </p>
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiCommunityFill/>	&nbsp; Company Name :	&nbsp; {input.company_name}  </p>
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiBuildingFill/>	&nbsp; Company City :	&nbsp; {input.company_city}  </p>
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiFileWord2Fill/> 	&nbsp; Job Type :	&nbsp; {input.job_type} </p>
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiCustomerService2Line/>	&nbsp; Job Qualification :	&nbsp; {input.job_qualification}  </p>
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiTerminalBoxFill/>	&nbsp; Job Description :	&nbsp; {input.job_description}  </p>
            
            <p className="pt-4 text-base font-bold text-green-700 flex items-center justify-center lg:justify-start"> <icon.RiMoneyDollarCircleFill/>	&nbsp; Salary Min IDR.	&nbsp; {input.salary_min} - Salary Max	&nbsp; IDR {input.salary_max}  </p>
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiMoonLine/> 	&nbsp; Job Status :	&nbsp; {input.job_status===1?"Dibuka" : "Ditutup"} </p>
            <div className="text-end">
           
          </div>
        
            
           
            
          </div>
        </div>
        {/*Img Col*/}
        <div className="w-full lg:w-2/5">
          {/* Big profile image for side bar (desktop) */}
          <img src={input.company_image_url} className="rounded-none lg:rounded-lg shadow-2xl hidden lg:block" />
          {/* Image from: http://unsplash.com/photos/MP0IUfwrn0A */}
        </div>
       
      </div>

      </Section>
        </>
    )
}


export default DetailJob