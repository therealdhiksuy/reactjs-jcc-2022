import React from "react";
import styledComponents from "styled-components";
import Cookies from "js-cookie";
import { Link } from "react-router-dom";

const Section = styledComponents.div`
background-image: url(https://images.unsplash.com/photo-1531685250784-7569952593d2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
background-repeat: no-repeat;
background-size: cover;
`

const Profile = () => {
    let user = JSON.parse(Cookies.get('user'))
    let image= user.image_url
    let name= user.name
    let email= user.email
    return (

        <>
<Section>
<div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
        {/*Main Col*/}
        <div id="profile" className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
          <div className="p-4 md:p-12 text-center lg:text-left">
            {/* Image for mobile view*/}
           
            <h1 className="text-3xl font-bold pt-8 lg:pt-0">{name}</h1>
            <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-green-500 opacity-25" />
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"><svg className="h-4 fill-current text-green-700 pr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9 12H1v6a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v1h4a2 2 0 0 1 2 2v6h-9V9H9v2zm3-8V2H8v1h4z" /></svg> {email}</p>
        
            
            <div className="pt-12 pb-8">
              <Link to ='/change-password'> <button className="bg-green-700 hover:bg-green-900 text-white font-bold py-2 px-4 rounded-full">
                Change Password
              </button>  </Link>
            </div>
            
          </div>
        </div>
        {/*Img Col*/}
        <div className="w-full lg:w-2/5">
          {/* Big profile image for side bar (desktop) */}
          <img src={image} className="rounded-none lg:rounded-lg shadow-2xl hidden lg:block" />
          {/* Image from: http://unsplash.com/photos/MP0IUfwrn0A */}
        </div>
       
      </div>
      </Section>
        </>
    )
}

export default Profile