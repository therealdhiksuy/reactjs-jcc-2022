import React, { useContext, useEffect } from "react";
import hero from "../asset/img/hero.png"
import landimg from "../asset/img/imgpage.png"
import styled from 'styled-components'
import { motion } from "framer-motion";
// import '../asset/style.css'
import { Link } from "react-router-dom";
import axios from "axios";
import { JobContext } from "../context/JobContext";
import Cookies from "js-cookie";


const Section = styled.section`
background: rgb(63,94,251);
background: linear-gradient(90deg, rgba(63,94,251,1) 9%, rgba(110,60,90,1) 100%);
height:auto;

`;
const Button = styled.div`
background: linear-gradient(90deg, #d53369 0%, #daae51 100%);


`;
    

const LandingPage = () => {
  const {state,handleFunction} = useContext(JobContext)
  const {
    dataJob,setdataJob,
    currentId, setCurrentId,
    fetchStatus ,setFetchStatus,
    input, setInput ,search,setSearch
  } = state

  const {
 
    handleEdit,
    handleDetail,
    handleDelete,handleSearch,handleChangeSearch
  } = handleFunction

  useEffect(()=>{

    let fetchData = async () => {
        let {data} = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
          let result = data.data.map((res)=>{
            
        let{ 
          id,company_city,
        company_image_url,
        company_name,
        job_description,
        job_qualification,
        job_status,
        job_tenure,
        job_type,
        salary_max,
        salary_min,
        title



 } = res
        return {
           id, company_city,
            company_image_url,
            company_name,
            job_description,
            job_qualification,
            job_status,
            job_tenure,
            job_type,
            salary_max,
            salary_min,
            title,
        
        }
    })
    
      setdataJob([...result])
      
       
    }
    if (fetchStatus){
        fetchData()
        setFetchStatus(false)

    }
    
  },[fetchStatus,setFetchStatus])
  const handleText = (text,max)  =>{
    if(text === null) {
        return ""
    }else {
        return text.slice(0,max) +"..."
    }


}


    return (<>
    
    
  <Section>
  <div>
      
        {/*Hero*/}
        <div className="relative pt-10">
          <div className="container px-10 mx-auto flex flex-wrap flex-col md:flex-row items-center">
            {/*Left Col*/}
            <div className="flex flex-col w-full md:w-2/5 justify-center items-start text-center md:text-left">
             
              <h1 className="my-4 text-5xl font-bold leading-tight">
              Getting job vacancies is easier
              </h1>
              <p className="leading-normal text-2xl mb-8">
              Many choices of job vacancies
              </p>
              {!Cookies.get('token')&&
              <Button className="animate-bounce mx-auto lg:mx-0  bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
                <Link to="/login">Login Now !!</Link>
              </Button>}
            </div>
            {/*Right Col*/}
            
            <div className="w-full md:w-3/5 py-7 text-center ">
          
              <img className="w-full md:w-4/5 z-50 ml-12" src={landimg} />
            </div>
          </div>
        </div>
       
        
      
        <div className="w-full bg-white p-12">
        <div className="header flex items-end justify-between mb-12">
          <div className="title">
            <p className="text-4xl font-bold text-gray-800 mb-4">
              Lastest Vacancy
            </p>
            <p className="text-2xl font-light text-gray-400">
              All vacancy are verified by 2 experts and valdiate by the CTO
            </p>
          </div>
          
          <div className="text-end">
            <form method="post" onSubmit={handleSearch} className="flex flex-col md:flex-row w-3/4 md:w-full max-w-sm md:space-x-3 space-y-3 md:space-y-0 justify-center">
              <div className=" relative ">
                <input type="text" id="&quot;form-subscribe-Search" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Search"   onChange={handleChangeSearch} value={search}/>
              </div>
              <button className="flex-shrink-0 px-4 py-2 text-base font-semibold text-white bg-purple-600 rounded-lg shadow-md hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-500 focus:ring-offset-2 focus:ring-offset-purple-200" type="submit">
                Search
              </button>
            </form>
          </div>
        </div>
     

        <div className="grid grid-cols-1 md:grid-cols-3 xl:grid-cols-3 gap-12">
    {
      dataJob.map((res,index)=>{
        return(
          <>
          
                  
          
            <div  className="overflow-hidden shadow-lg rounded-lg h-90 w-60 md:w-80 cursor-pointer m-auto  " key={index}>
   
                <div className="w-full block h-full">
                    <img alt="blog photo" src={res.company_image_url} className="max-h-40 w-full object-cover"/>
                    <div className="bg-white dark:bg-gray-800 w-full p-4" >
                    <p className="text-black-500 text-md text-center font-medium"  >
                            {res.title} 
                        </p>
                        <p className="text-black-500 text-md text-center font-small"  >
                            {res.job_tenure} 
                        </p>
                        <div className="m-5"></div>
                    <p className="text-indigo-500 text-md font-medium">
                            {res.company_name}
                        </p>

                        <p className="text-black-200 text-md font-medium">
                            {res.company_city}
                        </p>
                        <p className="text-gray-800 dark:text-white text-xl font-medium mb-2">
                           { res.job_type}
                        </p>
                        <p className="text-gray-700 dark:text-gray-300 font-light text-md text-justify">
                           Qualification : {handleText(res.job_qualification,100)}
                        </p>
                        <p className="text-gray-700 dark:text-gray-300 font-light text-md text-justify">
                        Description : {handleText(res.job_description,100)}
                        </p>
                        <div className="flex items-center mt-4">
                           
                            <div className="flex flex-col justify-between text-sm">
                                <p className="text-red-600/100 dark:text-white">
                                  IDR  {res.salary_min} - IDR {res.salary_max}
                                </p>
                                <p className="text-green-700 dark:text-gray-300 mt-2">
                                {res.job_status ===1?"Dibuka":""}</p>
                                <p className="text-red-700 dark:text-gray-300 ">
                                {res.job_status !==1?"Ditutup":""}
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <button onClick={handleDetail} value={res.id} className="m-4 flex items-center p-4 px-6 py-2  transition ease-in duration-200 uppercase rounded-full hover:bg-gray-800 hover:text-white border-2 border-gray-900 focus:outline-none">
    
    Detail
</button>



                </div>
               
            </div>
            
            
          </>
        )
      })
    }
     </div>
    </div>


       
      
      </div>
  </Section>
  
    
    </>)


}


export default LandingPage