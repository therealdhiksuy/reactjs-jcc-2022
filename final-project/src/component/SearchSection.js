import axios from "axios";
import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import styledComponents from "styled-components";
import { JobContext } from "../context/JobContext";
import * as icon from "react-icons/ri";
import { data } from "autoprefixer";


const Section = styledComponents.div`
height:auto;
background-image: url(https://images.unsplash.com/photo-1531685250784-7569952593d2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80);
background-repeat: no-repeat;
background-size: cover;
`
const SearchSection = () => {
    let {valueOfSearch} = useParams()
    

    let {slug} = useParams()
    let history = useHistory()
 const {state,handleFunction} = useContext(JobContext)

 const {
    dataJob,setdataJob,
   currentId, setCurrentId,
   fetchStatus ,setFetchStatus,
   input, setInput
 } = state
 const {

     handleChange,

     handleSubmitForm15
   
   } = handleFunction
   useEffect(()=>{
       const fetchSearch =async()=>{

        let result = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
        let datares=result.data.data
        let filterData = datares.filter((e)=>{
            return Object.values(e).join(" ").toLowerCase().includes(valueOfSearch.toLowerCase())
            
        })
        setdataJob([...filterData])
       
       }
       

       if(fetchStatus){{
           fetchSearch()
               setFetchStatus(false)
           
       }}
     
     
   },[fetchStatus,setFetchStatus])
   console.log(dataJob)
    return (

        <>
          <Section>
      
                    {

                        dataJob.map ((a) => {
                            return (
                                <>
                                
                                <div className="max-w-4xl flex items-center h-auto  max-h-full lg:h-screen flex-wrap mx-auto my-32 lg:my-0" >
                    {/*Main Col*/}
                    <div id="profile" className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
                      <div className="p-4 md:p-12 text-center lg:text-left">
                        {/* Image for mobile view*/}
                       
                        <h1 className="text-3xl font-bold pt-8 lg:pt-0 text-justify">{a.title}</h1>
                        <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-green-500 opacity-25" />
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"><icon.RiTimeFill/> 	&nbsp; Job Tenure :	&nbsp; {a.job_tenure}  </p>
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiCommunityFill/>	&nbsp; Company Name :	&nbsp; {a.company_name}  </p>
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiBuildingFill/>	&nbsp; Company City :	&nbsp; {a.company_city}  </p>
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiFileWord2Fill/> 	&nbsp; Job Type :	&nbsp; {a.job_type} </p>
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiCustomerService2Line/>	&nbsp; Job Qualification :	&nbsp; {a.job_qualification}  </p>
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiTerminalBoxFill/>	&nbsp; Job Description :	&nbsp; {a.job_description}  </p>
                        
                        <p className="pt-4 text-base font-bold text-green-700 flex items-center justify-center lg:justify-start"> <icon.RiMoneyDollarCircleFill/>	&nbsp; Salary Min IDR.	&nbsp; {a.salary_min} - Salary Max	&nbsp; IDR {a.salary_max}  </p>
                        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start"> <icon.RiMoonLine/> 	&nbsp; Job Status :	&nbsp; {a.job_status===1?"Dibuka" : "Ditutup"} </p>
                        <div className="text-end">
                      
                      </div>
                    
                        
                       
                        
                      </div>
                    </div>
                    {/*Img Col*/}
                    <div className="w-full lg:w-2/5">
                      {/* Big profile image for side bar (desktop) */}
                      <img src={a.company_image_url} className="rounded-none lg:rounded-lg shadow-2xl hidden lg:block" />
                      {/* Image from: http://unsplash.com/photos/MP0IUfwrn0A */}
                    </div>
                   
                  </div>
                                </>
                            )
                                
                        })
                    }
        
                  

            
        

      
       
     
           
      </Section>
        </>
    )
}


export default SearchSection