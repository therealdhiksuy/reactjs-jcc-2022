import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components'
import icon from "../asset/img/icon.svg"
import Cookies from 'js-cookie'
import * as FaIcons from 'react-icons/fa'
import * as AaIcons from 'react-icons/ai'
import * as Icons from "react-icons/hi";


const Navbar = ({toogle}) => {

    const Section = styled.div`
    list-style-type: none;
    
    `
    const Button = styled.div`



`;

const [sidebar,setSidebar] = useState()

const showSidebar = () => setSidebar(!sidebar)
    return (

        <>
        {/* <Section>
          
     <nav className="flex justify-between items-center h-20 wh-auto  text-black relative shadow-sm font-mono" role='navigation'>
     {Cookies.get('token') &&  <div className=" mx-8 bg-none text-lg">
     
            
      <Link to='/dashboard' className="menu-bars" >  <button  type="button" class="py-2 px-4 flex justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
        Dashboard </button> </Link>
            
           
             </div> }
             

           
     <Link to="/" className="pl-10">Project Akhir</Link> 
          
        <div className="pr-10 md:block hidden">
         {!Cookies.get('token')&&<Link className="p-4" to="/login">Login</Link>}
         {!Cookies.get('token')&&<Link className="p-4" to="/register">Register</Link>}
         {Cookies.get('token') && <li className="p-4"><span onClick={() => {
                    Cookies.remove('token')
                    window.location = "/login"

                }}> <Button className="p-4">
               Logout
              </Button></span></li>}
        </div>
        </nav>
        </Section> */}
   <Section>
<nav
      className='flex justify-between items-center h-16 bg-white text-black relative shadow-sm font-mono'
      role='navigation'
    > 
   <Link to='/' className='pl-8'>
        <Icons.HiAcademicCap/> Job Vacancy
      </Link>   
    <ul className="inline-flex">
     {Cookies.get('token') &&  <div className=" mx-8 bg-none text-lg">
     
       
     <Link to='/dashboard' className="menu-bars" >  <button  type="button" class="py-2 px-4 flex justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
       Dashboard </button> </Link>
           
          
            </div> }
            {Cookies.get('token') &&  <div className=" mx-8 bg-none text-lg">
     
            
     <Link to='/' className="menu-bars" >  <button  type="button" class="py-2 px-4  justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
       Home </button> </Link>
           
          
            </div> }
            {!Cookies.get('token') &&  <div className=" mx-8 bg-none text-lg">
     
            
     <Link to='/' className="menu-bars" >  <button  type="button" class="py-2 px-4  justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
       Home </button> </Link>
           
          
            </div> }
    
            </ul>
         
            
           
            
            
            
      <div className='px-4 cursor-pointer md:hidden' onClick={toogle}>
        <svg
          className='w-8 h-8'
          fill='none'
          stroke='currentColor'
          viewBox='0 0 24 24'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            strokeLinecap='round'
            strokeLinejoin='round'
            strokeWidth='2'
            d='M4 6h16M4 12h16M4 18h16'
          />
        </svg>
      </div>
   
      <div className='pr-8 md:block  hidden'>
          
      {!Cookies.get('token')&&<Link className="p-4" to="/login">Login</Link>}
    
         {!Cookies.get('token')&&<Link className="p-4" to="/register">Register</Link>}
         {Cookies.get('token') && <li className="p-4"><span onClick={() => {
                    Cookies.remove('token')
                    window.location = "/login"

                }}>
               Logout
              </span></li>}

      </div>
     
    </nav>
    </Section>
        </>
    )
}


export default Navbar