import React from "react";
import { Link } from "react-router-dom";
import Cookies from 'js-cookie'
import styledComponents from "styled-components";

const Section = styledComponents.div`
list-style-type: none;
`

const Button = styledComponents.div`



`;

const Dropdown  = ({isOpen,toogle}) => {
    return (


        <>
        <Section>
        
        <div className={isOpen ? 'grid grid-rows-1 text-center items-center bg-yellow-500' : 'hidden'}
        onClick={toogle}>



{!Cookies.get('token')&&<Link className="p-4" to="/login">Login</Link>}
         {!Cookies.get('token')&&<Link className="p-4" to="/register">Register</Link>}
         {Cookies.get('token') && <li className="p-4"><span onClick={() => {
                    Cookies.remove('token')
                    window.location = "/login"

                }}> <Button className="p-4">
               Logout
              </Button></span></li>}
        </div>
        </Section>
        </>
    )
}

export default Dropdown