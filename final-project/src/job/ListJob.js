import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { JobContext } from "../context/JobContext";
import styled from 'styled-components'

const Section = styled.div`
background-image: url(https://images.unsplash.com/photo-1530543787849-128d94430c6b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80);
background-repeat: no-repeat;
background-size: cover;
height:auto
`

const ListJob= ()=> {
 
    const {state,handleFunction} = useContext(JobContext)
    const {
      dataJob,setdataJob,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state

    const {
   
      handleEdit,
      
      handleDelete
    } = handleFunction

      useEffect(()=>{

        let fetchData = async () => {
            let {data} = await axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
              let result = data.data.map((res)=>{
            let{ 
              id,company_city,
            company_image_url,
            company_name,
            job_description,
            job_qualification,
            job_status,
            job_tenure,
            job_type,
            salary_max,
            salary_min,
            title



     } = res
            return {
               id, company_city,
                company_image_url,
                company_name,
                job_description,
                job_qualification,
                job_status,
                job_tenure,
                job_type,
                salary_max,
                salary_min,
                title,
            
            }
        })
        
          setdataJob([...result])
          console.log(result)  
           
        }
        if (fetchStatus){
            fetchData()
            setFetchStatus(false)

        }
        
      },[fetchStatus,setFetchStatus])
      
     
    const handleText = (text,max)  =>{
        if(text === null) {
            return ""
        }else {
            return text.slice(0,max) +"..."
        }


    }
    const[value,setValue ] = useState('')
    const [tableFilter,setTableFilter] =useState([])
 

    const filterData = (e) =>{
      if(e.target.value !== ""){
        setValue(e.target.value);
        const filterTable = dataJob.filter(o=>Object.keys(o).some(k=>
          String(o[k]).toLowerCase().includes(e.target.value.toLowerCase())))
setTableFilter([...filterTable])
      }else{
        setValue(e.target.value);
        setdataJob([...dataJob])
      }
    }

    return(

        <>
        <Section>
        <div>
   <div className=" flex  items-center justify-center">
  <Link to='/dashboard/list-job-vacancy/form/create'><button type="button" className="mt-10 py-4 px-6  bg-blue-600 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
    Create Data
</button>
</Link>
<div className=" relative ml-5 mt-10">
                <input type="text" id="&quot;form-subscribe-Search" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Search" onChange={filterData}  value={value} />
              </div>
</div>
        {/* <div className="">
            <h1>Daftar Nilai Mahasiswa</h1>
        <table id="">
  <thead>

  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Mata Kuliah</th>
    <th>Nilai</th>
    <th>Indeks Nilai</th>
    <th>Aksi</th>
  </tr>

  </thead>
  <tbody>
  
 
      {
      dataMahasiswa.map((res,index)=>{
          return (

          
            <tr key={index}>
    <td>{index +1}</td>
    <td>{res.name}</td>
    <td>{res.course}</td>
    <td>{res.score}</td>
    <td>{handleIndexScore(res.score)}</td>
  
    <td> 
      <div className="mt-20 min-g-screen flex items-center justify-center" >
        <button onClick={handleEdit} value={res.id}>Edit</button>
        <button onClick={handleDelete} value={res.id}>Delete</button>
        </div>
    </td>
    
  </tr>
           
          )
      })
  }
 
  </tbody>

</table> */}


        {/* </div> */}
      <div className=" min-g-screen flex items-center justify-center">
        <table className="m-20 table p-4 bg-white shadow rounded-lg">
        <thead>
          <tr>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
             No
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Tittle
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Job Description
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Job Qualification
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Job Type
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Job Tenure
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Job Status
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Company Name
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Company Image Url
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Company City
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Salary Min
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Salary Max
            </th>
           
            
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
      {
      value.length>0? tableFilter.map((res,index)=>{
          return (

          <tr className="text-gray-700" key={index}>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {index +1}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.title}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handleText(res.job_description,40)}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handleText(res.job_qualification,40)}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.job_type}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.job_tenure}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.job_status ===1?"Dibuka":"Ditutup"}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.company_name}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {handleText(res.company_image_url,20)}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.company_city}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.salary_min}
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
            {res.salary_max}
            </td>
            
            <td className="border-b-2 p-4 dark:border-dark-5">
            <button  className="m-2 py-2 px-4  bg-yellow-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleEdit} value={res.id}>Edit</button>
            <button className="m-2  py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleDelete} value={res.id}>Delete</button>
            
            </td>
          </tr>
           )
          })
      :
      dataJob.map((res,index)=>{
        return (

        <tr className="text-gray-700" key={index}>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {index +1}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.title}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {handleText(res.job_description,40)}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {handleText(res.job_qualification,40)}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.job_type}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.job_tenure}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.job_status ===1?"Dibuka":"Ditutup"}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.company_name}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {handleText(res.company_image_url,20)}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.company_city}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.salary_min}
          </td>
          <td className="border-b-2 p-4 dark:border-dark-5">
          {res.salary_max}
          </td>
          
          <td className="border-b-2 p-4 dark:border-dark-5">
          <button  className="m-2 py-2 px-4  bg-yellow-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleEdit} value={res.id}>Edit</button>
          <button className="m-2  py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"onClick={handleDelete} value={res.id}>Delete</button>
          
          </td>
        </tr>
         )
        })
      
      } 
        </tbody></table>
        
        </div>
        </div>
        </Section>
        </>
    )

}
export default ListJob