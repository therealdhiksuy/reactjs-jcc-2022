import axios from "axios"
import React, { useContext, useEffect, useState } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import { JobContext } from "../context/JobContext";
import styled from 'styled-components'

const Section = styled.section`
background-image: url(https://images.unsplash.com/photo-1531315630201-bb15abeb1653?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80);
background-repeat: no-repeat;
background-size: cover;
`

const JobForm= ()=> {
 
 
        let {slug} = useParams()
       let history = useHistory()
    const {state,handleFunction} = useContext(JobContext)
 
    const {
      dataMahasiswa,setDataMahasiswa,
      currentId, setCurrentId,
      fetchStatus ,setFetchStatus,
      input, setInput
    } = state
    const {

        handleChange,

        handleSubmitForm15
      
      } = handleFunction
      useEffect(()=>{

        if(slug !== undefined ){
            axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${slug}`)
        .then((res)=>{
           let data = res.data
                console.log(data)
            setInput({
                company_city:data.company_city,
            company_image_url:data.company_image_url,
            company_name:data.company_name,
            job_description:data.job_description,
            job_qualification:data.job_qualification,
            job_status:data.job_status,
            job_tenure:data.job_tenure,
            job_type:data.job_type,
            salary_max:data.salary_max,
            salary_min:data.salary_min,
            title:data.title

              }

            )
            setCurrentId(data.id)

           
        })

        .catch((err)=>{
            console.log(err)
        })
        }
        return ()=>{
            setInput({
                company_city: "",
                company_image_url: "",
                company_name: "",
                job_description: "",
                job_qualification: "",
                job_status: 1,
                job_tenure: "",
                job_type: "",
                salary_max: 0,
                salary_min: 0,
                title: "",
              }

            )
           
        }
        
      },[])
     
     
      
      

    return(

        <>
        
        
        <Section>
        <div className="container-form">

            

    {/* <label >Nama :</label>
    <input onChange={handleChange} value={input.name} type="text"  name="name" placeholder="Nama"required/>
    <label >Mata Kuliah :</label>
    <input onChange={handleChange} value={input.course} type="text"  name="course" placeholder="Mata Kuliah" required/>
    <label >Nilai : </label>
    <input onChange={handleChange} value={input.score} type="number"  name="score" min={0} max={100}  placeholder="Nilai"  required/> */}
      <form onSubmit={handleSubmitForm15}>
      <div className="flex items-stretch">
<div className="relative m-20 bg-white rounded-lg shadow sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden ring-offset-2 ring">
        <div className="px-4 py-8 sm:px-10">
          <div className="relative mt-6">
            <div className="absolute inset-0 flex items-center">
              <div className="w-full border-t border-gray-300">
              </div>
            </div>
            <div className="relative flex justify-center text-sm leading-5">
              <span className="px-2 text-gray-500 bg-white">
                Form Job
              </span>
            </div>
          </div>
          <div className="mt-6">
            <div className="w-full space-y-6">
              <div className="w-full">
                <div className=" relative ">
                <span className="px-2 text-gray-500 bg-white">
               Tittle
              </span>
              <input onChange={handleChange} value={input.title} type="text"  name="title" id="search-form-location" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Job Description
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.job_description} type="text"  name="job_description" id="search-form-location" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="" required/>
                </div>
              </div>
              <span className="px-2 text-gray-500 bg-white">
               Job Qualification
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.job_qualification} type="text"  name="job_qualification" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Job Type
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.job_type} type="text"  name="job_type" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent "  placeholder="" required/>
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Job Tenure
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.job_tenure} type="text"  name="job_tenure" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder=""required />
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Job Status
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.job_status} type="checkbox"  name="job_status" id="search-form-name" className=""  checked={input.job_status}/> Dibuka
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Company Name
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.company_name} type="text"  name="company_name" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"  placeholder=""  requiered/>
                </div>
              </div>
             
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Company Image Url
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.company_image_url} type="text"  name="company_image_url" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder=""required />
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Company City
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.company_city} type="text"  name="company_city" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder=""required />
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Salary Min
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.salary_min} type="number"  name="salary_min" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder=""required />
                </div>
              </div>
              <span className="mt-5 px-2 text-gray-500 bg-white">
               Salary Max
              </span>
              <div className="w-full">
                <div className=" relative ">
                  <input onChange={handleChange} value={input.salary_max} type="number"  name="salary_max" id="search-form-name" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder=""required />
                </div>
              </div>
              <div>
                  
                <span className="block w-full rounded-md shadow-sm">
                
                  
<button type="submit" value="submit" className="py-2 px-4 flex justify-center items-center  bg-green-500 hover:bg-green-700 focus:ring-green-500 focus:ring-offset-green-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full">
    <svg width="20" height="20" className="mr-2" fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
        <path d="M1344 1472q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm256 0q0-26-19-45t-45-19-45 19-19 45 19 45 45 19 45-19 19-45zm128-224v320q0 40-28 68t-68 28h-1472q-40 0-68-28t-28-68v-320q0-40 28-68t68-28h427q21 56 70.5 92t110.5 36h256q61 0 110.5-36t70.5-92h427q40 0 68 28t28 68zm-325-648q-17 40-59 40h-256v448q0 26-19 45t-45 19h-256q-26 0-45-19t-19-45v-448h-256q-42 0-59-40-17-39 14-69l448-448q18-19 45-19t45 19l448 448q31 30 14 69z">
        </path>
    </svg>
    Submit
</button>
<button type="button" className="mt-5 py-2 px-4 flex justify-center items-center  bg-red-500 hover:bg-red-700 focus:ring-red-500 focus:ring-offset-red-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-full"><Link to='/dashboard/list-job-vacancy' >Kembali ke tabels</Link></button>

                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="px-4 py-6 border-t-2 border-gray-200 bg-gray-50 sm:px-10">
          <p className="text-xs leading-5 text-gray-500">
           Form Job
          </p>
        </div>
      </div>
    

    
  
 </div>
  </form>
</div>
</Section>
        </>
    )

}
export default JobForm