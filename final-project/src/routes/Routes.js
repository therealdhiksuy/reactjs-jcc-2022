import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import { Switch } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";
import Login from "../auth/Login";
import Register from "../auth/Register";
import Dropdown from "../component/Dropdown";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";

import Dashboard from "../home/Dashboard";
import LandingPage from "../home/LandingPage";
import Cookies from 'js-cookie'
import Sidebar from "../component/Sidebar";
import Sidebar1 from "../component/Sidebar1";
import ListJob from "../job/ListJob";
import { JobProvider } from "../context/JobContext";
import JobForm from "../job/FormJob";
import { Redirect } from "react-router-dom";
import Profile from "../home/Profile";
import DetailJob from "../home/DetailJob";
import SearchSection from "../component/SearchSection";
import Changepass from "../auth/Changepass";
import NotFound from "../component/NotFound";



const Routes = () => {

    const [isOpen,setIsOpen] = useState(false)
    const toogle = () => {
        setIsOpen(!isOpen);
    };

    const LoginRoute = ({...props}) => {
        if(Cookies.get('token') !==undefined){
            return <Redirect to={'/dashboard'}/>
        } else if (Cookies.get('token')===undefined){
            return <Route {...props}/>
        }
    }
    

useEffect(() => {


    const hideMenu = () => {

        if(window.innerWidth >768 && isOpen) {
            setIsOpen(false)
        }
    }
    window.addEventListener('resize' , hideMenu)

    return () => {
        window.removeEventListener ('resize ' , hideMenu);
    }
})


    return (


        <>
 <BrowserRouter>
 <JobProvider>
 <Navbar toogle={toogle}/>
 <Dropdown isOpen={isOpen} toogle={toogle}/>
 <Switch>


 
  <Route path={'/'} exact component={LandingPage} />
  <LoginRoute path={'/login'} exact component={Login} />
  <Route path={'/register'} exact component={Register} />

  <Route path={'/dashboard'} exact component={Dashboard} />
  <Route path={'/dashboard/list-job-vacancy'} exact component={ListJob} />

 
  <Route path={'/dashboard/list-job-vacancy/form/create'} exact component={JobForm} />
  <Route path={'/dashboard/list-job-vacancy/form/edit/:slug'} exact component={JobForm} />
  <Route path={'/profile'} exact component={Profile} />
  <Route path={'/list-job-vacancy/detail/:slug'} exact component={DetailJob} />
  <Route path={'/search/:valueOfSearch'} exact component={SearchSection} />
  <Route path={'/change-password'} exact component={Changepass} />
  <Route exact component={NotFound}/>
  

        
 </Switch>
 
 <Footer/>
 </JobProvider>
 </BrowserRouter>
        
        </>
    )
}




export default Routes